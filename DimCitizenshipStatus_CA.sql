		-- Merge new & modified data
			-- for this table no need to maintain SCD
		MERGE INTO EDW_REP.dbo.DimCitizenshipStatus AS Target
		 USING edw_staging.dbo.StgCitizenshipStatus AS Source
			ON Target.CitizenshipStatus_BKEY = Source.CitizenshipStatus_BKEY
			and Target.DataSourceId = Source.DataSourceId

		WHEN MATCHED AND Target.CitizenshipStatus <> Source.CitizenshipStatus
		THEN
		UPDATE SET  Target.CitizenshipStatus = Source.CitizenshipStatus
				  , Target.LookupType_BKEY = Source.LookupTypeID
				 -- , Target.DataSourceId = Source.DataSourceId
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
     
		WHEN NOT MATCHED 
		THEN
		INSERT  
				   (CitizenshipStatus_BKEY
				   ,CitizenshipStatus
				   ,DataSourceId
				   ,W_Insert_Dt
				   ,W_Update_Dt
				   ,Etl_Run_Id
				   ,Current_flg
				   ,LookupType_BKEY)
		VALUES
				   (Source.CitizenshipStatus_BKEY
				   ,Source.CitizenshipStatus
				   ,Source.DataSourceId
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,1
				   ,Source.LookupTypeID)
			;
	    
		--Updating LookupType_WID
		UPDATE  A
		SET A.LookupType_WID = LG.DWLookupType_WID
		FROM EDW_REP.dbo.DimCitizenshipStatus A
		INNER JOIN 	dbo.DIMDWLookupType LG
			ON A.LookupType_BKEY = LG.DWLookupTypeId_BKEY and A.DataSourceId=LG.DataSourceId
			WHERE A.Etl_Run_Id = @EDWLogID






