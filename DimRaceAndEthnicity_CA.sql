		-- Merge new & modified data
			-- for this table no need to maintain SCD
		MERGE INTO EDW_REP.dbo.DimRaceAndEthnicity AS Target
		 USING edw_staging.dbo.STGRaceAndEthnicity AS Source
			ON Target.RaceAndEthnicity_BKEY = Source.RaceAndEthnicity_BKEY
			AND Target.DataSourceId = Source.DataSourceId

		WHEN MATCHED AND (Target.RaceAndEthnicity <> CASE WHEN (Source.DataSourceId = 1 AND Source.SortIdx = 1 AND Source.RaceAndEthnicity = 'Yes') THEN 'Hispanic' ELSE Source.RaceAndEthnicity END
							OR Target.Sortidx <> Source.Sortidx	)
		THEN
		UPDATE SET  Target.RaceAndEthnicity = CASE WHEN (Source.DataSourceId = 1 AND Source.SortIdx = 1 AND Source.RaceAndEthnicity = 'Yes') THEN 'Hispanic' ELSE Source.RaceAndEthnicity end
				  , Target.LookupType_BKEY = Source.LookupTypeID
				  , Target.Sortidx = Source.Sortidx
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
     
		WHEN NOT MATCHED 
		THEN
		INSERT  
				   (RaceAndEthnicity_BKEY
				   ,RaceAndEthnicity
				   ,Sortidx
				   ,DataSourceId
				   ,W_Insert_Dt
				   ,W_Update_Dt
				   ,Etl_Run_Id
				   ,LookupType_BKEY)
		VALUES
				   (Source.RaceAndEthnicity_BKEY
				   ,CASE WHEN (Source.DataSourceId = 1 AND Source.SortIdx = 1 AND Source.RaceAndEthnicity = 'Yes') THEN 'Hispanic' ELSE Source.RaceAndEthnicity end
				   ,Source.Sortidx
				   ,Source.DataSourceId
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,Source.LookupTypeID)
			;
	    
		--Updating LookupType_WID
		UPDATE  A
		SET A.LookupType_WID = LG.DWLookupType_WID
		FROM EDW_REP.dbo.DimRaceAndEthnicity A
		INNER JOIN 	dbo.DIMDWLookupType LG
			ON A.LookupType_BKEY = LG.DWLookupTypeId_BKEY AND A.DataSourceId=LG.DataSourceId
			where A.Etl_Run_Id = @EDWLogID;

		