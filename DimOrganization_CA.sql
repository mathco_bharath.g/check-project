		-- Merge new & modified data
			-- we can enable SCD for a subset of the columns if needed (where historical changes need to be preserved)
			-- If condition added to handle the 2 different merge conditions that are to be handled one each for Common App and CAS 3
			-- IsActive column needs to be updated only for CAS3 but not for CA because it is applicable only for CAS3
	IF (@ProcessName = 'CA EDW2.0 ETL')
    Begin
		MERGE INTO EDW_REP.dbo.DimOrganization AS Target
		 USING edw_staging.dbo.StgOrganization AS Source
			ON Target.Organization_BKEY = Source.Organization_BKEY
			and Target.CAS_BKEY  = Source.CAS_BKEY 
			and Target.DataSourceId = Source.DataSourceId
		WHEN MATCHED AND (Target.OrganizationName <> Source.OrganizationName OR COALESCE(Target.EtsCode, '') <> COALESCE(Source.EtsCode, '') OR COALESCE(Target.Organization_ExternalID, '') <> COALESCE(Source.Organization_ExternalID, '') OR COALESCE(Target.Institution_BKEY, '') <> COALESCE(Source.Institution_BKEY, ''))
		THEN
		UPDATE SET  Target.OrganizationName = Source.OrganizationName
				  , Target.EtsCode  = Source.EtsCode
				  ,Target.Organization_ExternalID = Source.Organization_ExternalID
				  --,Target.DataSourceId = Source.DataSourceId
				  , Target.Institution_BKEY  = Source.Institution_BKEY
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID     
		WHEN NOT MATCHED BY TARGET
		THEN
		INSERT  
				   (Organization_BKEY
				   ,OrganizationName
				   ,Organization_ExternalID
				   ,EtsCode
				   ,CAS_BKEY
				   ,DataSourceId
				   ,Institution_BKEY
				   ,W_Insert_Dt
				   ,W_Update_Dt
				   ,Etl_Run_Id
				   ,Current_flg)
		VALUES
				   (Source.Organization_BKEY
				   ,Source.OrganizationName
				   ,Source.Organization_ExternalID
				   ,Source.EtsCode
				   ,Source.CAS_BKEY
				   ,Source.DataSourceId
				   ,Source.Institution_BKEY
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,1
				   );
	END
    ELSE
    BEGIN
		MERGE INTO EDW_REP.dbo.DimOrganization AS Target
		 USING edw_staging.dbo.StgOrganization AS Source
			ON Target.Organization_BKEY = Source.Organization_BKEY
			and Target.DataSourceId = Source.DataSourceId

		WHEN MATCHED              
		THEN
		UPDATE SET  Target.OrganizationName = Source.OrganizationName
				  , Target.EtsCode  = Source.EtsCode
				  , Target.Organization_ExternalID = Source.Organization_ExternalID
				  , Target.Institution_BKEY  = Source.Institution_BKEY
				  , Target.CAS_BKEY  = Source.CAS_BKEY 
				  ,Target.UniqueIdentifier  = Source.UniqueIdentifier 
				  --, Target.IsActive = 1 -- IsActive column needs to be updated only for CAS3 but not for CA because it is applicable only for CAS3
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
     
		WHEN NOT MATCHED 
		THEN
		INSERT
		(Organization_BKEY
				   ,OrganizationName
				   ,Organization_ExternalID
				   ,EtsCode
				   ,CAS_BKEY
				   ,UniqueIdentifier
				   ,DataSourceId
				   ,Institution_BKEY
				   --,IsActive
				   ,W_Insert_Dt
				   ,W_Update_Dt
				   ,Etl_Run_Id
				   ,Current_flg)
					   
		VALUES
				   (Source.Organization_BKEY
				   ,Source.OrganizationName
				   ,Source.Organization_ExternalID
				   ,Source.EtsCode
				   ,Source.CAS_BKEY
				   ,Source.UniqueIdentifier
				   ,Source.DataSourceId
				   ,Source.Institution_BKEY
				   --,1
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,1
				   );
	End


		-- update CAS_WID
		UPDATE  O
		SET O.CAS_WID = I.CAS_WID   -- select *
		FROM EDW_REP.dbo.DimOrganization O
		INNER JOIN 	EDW_REP.dbo.DimCASIdentifiers  I
			ON O.CAS_BKEY = I.CAS_BKEY and O.DataSourceId=I.DataSourceId
			WHERE O.Etl_Run_Id = @EDWLogID

		-- update Institution_WID  -  not applicable for the first release - CAS doesn't have the same Inst. concept as WA

		IF @ProcessName = 'ETL CAS 3'
        Begin
		  UPDATE  O
		  SET O.Institution_WID = I.Institution_WID
		  FROM EDW_REP.dbo.DimOrganization O
			  INNER JOIN 	EDW_REP.lia.DimInstitution  I
					ON O.Institution_bkey = I.Institution_BKEY
					AND O.DataSourceId = I.DataSourceId
		  WHERE O.Etl_Run_Id = @EDWLogID

		----CAS_BKEY 812 corresponds to Groot CAS and any organization that moves to Groot CAS needs to be 
		---- IsActive column needs to be updated only for CAS3 but not for CA because it is applicable only for CAS3
		--  UPDATE  O
		--  SET O.IsActive = 0
		--  FROM EDW_REP.dbo.DimOrganization O
		--  WHERE O.Etl_Run_Id = @EDWLogID AND O.DataSourceId = 1 AND o.CAS_BKEY = 812
		END

		-- update Institution_BKEY, Institution_WID and InstitutionName
		/*UPDATE o
		SET o.Institution_BKEY = i.Institution_BKEY,
		o.Institution_WID = i.Institution_WID,
		o.InstitutionName = i.InstitutionName
		FROM EDW_REP..DimOrganization o
		INNER JOIN EDW_Staging..TMPAssociationOrg a ON o.Organization_BKEY = a.OrganizationId
		INNER JOIN EDW_Staging..TMPorganizations_WA w ON w.AssociationOrgId = a.Id
		INNER JOIN EDW_REP..DimInstitution i ON w.institution_id = i.Institution_BKEY
		WHERE o.CAS_BKEY <> 1875 AND  (COALESCE(o.Institution_BKEY, 0) <> COALESCE(i.Institution_BKEY, 0) 
		OR COALESCE(o.Institution_WID, 0) <> COALESCE(i.Institution_WID, 0)
		OR COALESCE(o.InstitutionName, '') <> COALESCE(i.InstitutionName, ''))*/

		







