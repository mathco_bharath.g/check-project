		-- Merge new & modified data
			-- for this table no need to maintain SCD
		MERGE INTO EDW_REP.dbo.DimApplicantType AS Target
		 USING edw_staging.dbo.StgApplicantType AS Source
			ON Target.ApplicantType_BKEY = Source.ApplicantType_BKEY
				And Target.DataSourceId = Source.DataSourceId

		WHEN MATCHED              
		THEN
		UPDATE SET  Target.ApplicantType = Source.ApplicantType
				  , Target.LookupType_BKEY = Source.LookupTypeID
				  --, Target.DataSourceId = Source.DataSourceId
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
     
		WHEN NOT MATCHED 
		THEN
		INSERT  
				   (ApplicantType_BKEY
				   ,ApplicantType
				   ,LookupType_BKEY
				   ,DataSourceId
				   ,W_Insert_Dt
				   ,W_Update_Dt
				   ,Etl_Run_Id
				   ,Current_flg)
		VALUES
				   (Source.ApplicantType_BKEY
				   ,Source.ApplicantType
				   ,Source.LookupTypeID
				   ,Source.DataSourceId
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,1
				   )
			;
	
		--Updating LookupType_WID
		UPDATE  A
		SET A.LookupType_WID = LG.DWLookupType_WID
		FROM EDW_REP.dbo.DimApplicantType A
		INNER JOIN 	EDW_REP.dbo.DIMDWLookupType LG
			ON A.LookupType_BKEY = LG.DWLookupTypeId_BKEY and A.DataSourceId=LG.DataSourceId
			where A.Etl_Run_Id = @EDWLogID;

