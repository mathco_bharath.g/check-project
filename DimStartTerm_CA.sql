		-- Merge new & modified data
			-- for this table no need to maintain SCD
		MERGE INTO EDW_REP.dbo.DimStartTerm AS Target
		 USING edw_staging.dbo.STGStartTerm AS Source
			ON Target.AssociationInstance_BKEY = Source.AssociationInstance_BKEY
				and Target.Organization_BKEY = Source.Organization_BKEY
				AND Target.StartTerm = Source.StartTerm 
				AND Target.DataSourceId = Source.DataSourceId
		WHEN MATCHED             
		THEN
		UPDATE SET  Target.StartTerm = Source.StartTerm
				  --, Target.DataSourceId = Source.DataSourceId
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
     
		WHEN NOT MATCHED 
		THEN
		INSERT  
				   (StartTerm
				   ,AssociationInstance_BKEY
				   ,Organization_BKEY
				   ,DataSourceId
				   ,W_Insert_Dt
				   ,W_Update_Dt
				   ,Etl_Run_Id
				   ,Current_flg)
		VALUES
				   (Source.StartTerm
				   ,Source.AssociationInstance_BKEY
				   ,Source.Organization_BKEY
				   ,Source.DataSourceId
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,1
				   )
			;
	
		--UpdatingAssociationInstance_BKEY
		UPDATE  AI
		SET AI.AssociationInstance_WID = A.AssociationInstance_WID
		FROM EDW_REP.dbo.DimStartTerm AI
		INNER JOIN EDW_REP.dbo.DimAssociationInstance A
			ON AI.AssociationInstance_BKEY = A.Instance_BKEY AND AI.DataSourceId=A.DataSourceId
			WHERE AI.Etl_Run_Id = @EDWLogID
			and ai.Current_flg = 1;

		--UpdatingOrganization_BKEY
		UPDATE  AI
		SET AI.Organization_WID = A.Organization_WID
		FROM EDW_REP.dbo.DimStartTerm AI
		INNER JOIN EDW_REP.dbo.DimOrganization A
			ON AI.Organization_BKEY = A.Organization_BKEY AND AI.DataSourceId=A.DataSourceId
		INNER JOIN EDW_REP.dbo.DimAssociationInstance INST ON INST.Instance_BKEY = AI.AssociationInstance_BKEY AND INST.DataSourceId=AI.DataSourceId
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON CI.CAS_BKEY = INST.CAS_BKEY AND A.CAS_BKEY = CI.CAS_BKEY AND A.DataSourceId=CI.DataSourceId
		WHERE AI.Etl_Run_Id = @EDWLogID
		AND AI.Current_flg = 1;





