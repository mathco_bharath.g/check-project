		MERGE INTO EDW_REP.dbo.DimAssociationInstance AS Target
		 USING edw_staging.dbo.STGAssociationInstance AS Source
			ON Target.Instance_BKEY = Source.Instance_BKEY 
			AND Target.CAS_BKEY = Source.CAS_BKEY
			AND Target.DataSourceId = Source.DataSourceId
		WHEN MATCHED   
		THEN
		UPDATE SET  Target.FormId = Source.FormId
				  , Target.InstanceName = Source.InstanceName
				  , Target.AcademicYear = Source.AcademicYear
				  , Target.Active = Source.Active
				  , Target.BeginAcceptance = Source.BeginAcceptance
				  , Target.StartTerm = Source.StartTerm
				  , Target.SubmissionDeadline = Source.SubmissionDeadline
				  , Target.CycleName = Source.CycleName
				  , Target.CycleStartDate = Source.CycleStartDate
				  , Target.CycleEndDate = Source.CycleEndDate
				  --, Target.DataSourceId = Source.DataSourceId
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
     
		WHEN NOT MATCHED 
		THEN
		INSERT 
			(Instance_BKEY
				,FormId
				,InstanceName
				,AcademicYear
				,Active
				,BeginAcceptance
				,StartTerm
				,SubmissionDeadline
				,CAS_BKEY
				,CycleName
				,CycleStartDate
				,CycleEndDate
				,DataSourceId
				,W_Insert_Dt
				,W_Update_Dt
				,Etl_Run_Id
				,Effective_from_dt
				,Effective_to_dt
				,Current_flg)
		VALUES (Source.Instance_BKEY
				,Source.FormId
				,Source.InstanceName
				,Source.AcademicYear
				,Source.Active
				,Source.BeginAcceptance
				,Source.StartTerm
				,Source.SubmissionDeadline
				,Source.CAS_BKEY
				,Source.CycleName
				,Source.CycleStartDate
				,Source.CycleEndDate
				,Source.DataSourceId
				,'{{ DT }}'
				,'{{ DT }}'
				,@EDWLogID
				,'{{ DT }}'
				,NULL
				,1);

		--UpdatingAssociationInstance_BKEY
		UPDATE  AI
		SET AI.CAS_WID = CI.CAS_WID
		FROM EDW_REP.dbo.DimAssociationInstance AI
		INNER JOIN 	EDW_REP.dbo.DimCASIdentifiers CI
			ON AI.CAS_BKEY = CI.CAS_BKEY and AI.DataSourceId=CI.DataSourceId
		WHERE AI.Etl_Run_Id = @EDWLogID
		AND AI.Current_flg = 1;

		