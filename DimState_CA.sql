		-- Merge new & modified data
			-- for this table no need to maintain SCD
		MERGE INTO EDW_REP.dbo.DimState AS Target
		 USING edw_staging.dbo.StgState AS Source
			ON Target.State_BKEY = Source.State_BKEY
			AND Target.DataSourceId = Source.DataSourceId

		WHEN MATCHED AND (Target.State <> Source.State
				 OR Target.Abbreviation <> Source.Abbreviation)     
		THEN
		UPDATE SET  Target.State = Source.State
				  ,Target.Abbreviation = Source.Abbreviation
				  ,Target.LookupType_BKEY = Source.LookupTypeID
				  --,Target.DataSourceId = Source.DataSourceId
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
     
		WHEN NOT MATCHED 
		THEN
		INSERT  
				   (State_BKEY
					,State
					,Abbreviation
					,DataSourceId
					,W_Insert_Dt
					,W_Update_Dt
					,Etl_Run_Id
					,Current_flg
					,LookupType_BKEY)
		VALUES
				   (Source.State_BKEY
				   ,Source.State
				   ,Source.Abbreviation
				   ,Source.DataSourceId
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,1
				   ,Source.LookupTypeID)
			;
	
		--Updating LookupType_WID
		UPDATE  A
		SET A.LookupType_WID = LG.DWLookupType_WID
		FROM EDW_REP.dbo.DimState A
		INNER JOIN 	dbo.DIMDWLookupType LG
			ON A.LookupType_BKEY = LG.DWLookupTypeId_BKEY and A.DataSourceId=LG.DataSourceId
		where A.Etl_Run_Id = @EDWLogID;
