		-- Merge new & modified data
			-- for this table no need to maintain SCD
		MERGE INTO EDW_REP.dbo.DimCountry AS Target
		 USING edw_staging.dbo.StgCountry AS Source
			ON Target.Country_BKEY = Source.Country_BKEY
			AND  Target.DataSourceId = Source.DataSourceId

		WHEN MATCHED AND (Target.Country <> Source.Country
				  OR Target.CountryCode <> Source.CountryCode	
				  OR Target.ISOCode2 <> Source.ISOCode2
				  OR Target.ISOCode3 <> Source.ISOCode3)          
		THEN
		UPDATE SET  Target.Country = Source.Country
				  , Target.CountryCode = Source.CountryCode	
				  , Target.ISOCode2 = Source.ISOCode2
				  , Target.ISOCode3 = Source.ISOCode3
				  --, Target.DataSourceId = Source.DataSourceId
				  , Target.W_Update_Dt = '{{ DT }}'
				  , Target.ETL_Run_Id = @EDWLogID
				  , Target.LookupType_BKEY = Source.LookupTypeID
     
		WHEN NOT MATCHED 
		THEN
		INSERT  
				   (Country_BKEY
				   ,Country
				   ,CountryCode
				   ,ISOCode2
				   ,ISOCode3
				   ,DataSourceId
				   ,W_Insert_Dt
				   ,W_Update_Dt
				   ,Etl_Run_Id
				   ,Current_flg
				   ,LookupType_BKEY)
		VALUES
				   (Source.Country_BKEY
				   ,Source.Country
				   ,Source.CountryCode
				   ,Source.ISOCode2
				   ,Source.ISOCode3
				   ,Source.DataSourceId
				   ,'{{ DT }}'
				   ,'{{ DT }}'
				   ,@EDWLogID
				   ,1
				   ,Source.LookupTypeID)
			;
	
		--Updating LookupType_WID
		UPDATE  A
		SET A.LookupType_WID = LG.DWLookupType_WID
		FROM EDW_REP.dbo.DimCountry A
		INNER JOIN 	dbo.DIMDWLookupType LG
			ON A.LookupType_BKEY = LG.DWLookupTypeId_BKEY AND A.DataSourceId=LG.DataSourceId
		WHERE A.Etl_Run_Id = @EDWLogID;


		