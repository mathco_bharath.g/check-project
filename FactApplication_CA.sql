	
		--Insert into factApplicationTEMP to use it as the source of Insertation/Merge

		SET NOCOUNT  ON;

		INSERT  INTO EDW_REP.dbo.FactApplicationTemp
				   (Applicant_WID,
					AssociationInstance_WID,
					Organization_WID,
					Institution_WID,
					Program_WID,
					ProgramType_WID,
					Degree_WID,
					Major_WID,
					ApplicantType_WID,
					DecisionPlan_WID,
					FirstGenStatus_WID,
					FeeWaiverStatus_WID,
					StartTerm_WID,
					ApplicantStatus_WID,
					ApplicationStatus_WID,
					ApplicantDetails_WID,
					RaceAndEthnicity_WID,
					ApplicationYear,
					SubmittedDate_WID,
					CompletedDate_WID,
					VerifiedDate_WID,
					FirstSubmittedDate_WID,
					FirstCompletedDate_WID,
					FirstVerifiedDate_WID,
					Gender,
					Age,
					W_Insert_Dt,
					W_Update_Dt,
					Etl_Run_Id,
					CitizenshipStatus_WID,
					ResidenceCountry_WID,
					ResidenceState_WID,
					Applicant_ExternalId,
					GPA,
					GPAScale_WID,
					GPAWeight_WID,
					IntendedDegree_WID,
					PermanentHome_WID,
					CareerInterest_WID,
					DataSourceId
					)
			Select distinct
		a.Applicant_WID,
		ad.AssociationInstance_WID,
		o.Organization_WID,
		o.Institution_WID,
		NULL AS Program_WID,
		NULL AS ProgramType_WID,
		NULL AS Degree_WID,
		NULL AS Major_WID,
		ay.ApplicantType_WID,
		ad.DecisionPlan_WID,
		fgs.FirstGenStatus_WID,
		AD.FeeWaiver_WID FeeWaiver_WID,
		ad.StartTerm_WID,
		NULL AS ApplicantStatus_WID,
		NULL as ApplicationStatus_WID,
		ad.ApplicantDetails_WID,
		--CASE
  --                   WHEN ad.IsUSResident = 0 THEN (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceandEthnicity WHERE RaceAndEthnicity_BKEY = -9997) -- Nonresident Alien 
  --                   WHEN ad.IsHispanicOrLatino = 1 THEN (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceandEthnicity WHERE RaceAndEthnicity_BKEY = -2371) -- Hispanic or Latino
  --                   WHEN re.Ethnicity_WID IS NULL THEN (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceandEthnicity WHERE RaceAndEthnicity_BKEY = -9998) -- Did Not Report
  --                   ELSE re.Ethnicity_WID 
  --					END

		Null AS RaceAndEthnicity_WID,
		ai.CycleName AS ApplicationYear,
		d.Date_WID AS SubmittedDate_WID,
		NULL  AS  CompletedDate_WID,
		NULL  AS  VerifiedDate_WID,
		NULL  AS  FirstSubmittedDate_WID,
		NULL  AS  FirstCompletedDate_WID,
		NULL  AS  FirstVerifiedDate_WID,
		--UPPER(LEFT(AD.Gender,1))+LOWER(RIGHT(AD.Gender,LEN(AD.Gender) - 1)) as Gender,
		case when ad.gender <> '' then UPPER(LEFT(AD.Gender,1))+LOWER(RIGHT(AD.Gender,LEN(AD.Gender) - 1)) else  '' end as Gender,
		ad.Age,
		'{{ DT }}',
		'{{ DT }}',
		@EDWLogID,
		ad.CitizenshipStatus_WID AS CitizenshipStatus_WID,
		ad.CountryOfResidence_WID AS ResidenceCountry_WID,
		ad.StateofResidence_WID AS ResidenceState_WID,
		a.ExternalId as Applicant_ExternalId,
		ad.GPA,
		ad.GPAScale_WID,
		ad.GPAWeight_WID,
		ad.IntendedDegree_WID,
		ad.PermanentHome_WID,
		ad.CareerInterest_WID,
		ad.DataSourceId
FROM
		EDW_REP.dbo.DimApplicant a
		INNER JOIN EDW_REP.dbo.DimApplicantDetails ad ON ad.Applicant_WID = a.Applicant_WID 
		and ad.AssociationInstance_WID = a.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimOrganization o ON o.Organization_WID = ad.Organization_WID 
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = ad.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID and o.CAS_WID = CI.CAS_WID
		INNER JOIN EDW_REP.dbo.DimFeeWaiverStatus FWS ON AD.FeeWaiver_WID = FWS.FeeWaiverStatus_WID
		LEFT JOIN EDW_REP.dbo.DimApplicantType ay ON ay.ApplicantType_WID = a.ApplicantType_WID 
		LEFT JOIN EDW_REP.dbo.DimFirstGenStudentStatus fgs ON fgs.FirstGenStatus_WID = ad.FirstGenStudentStatus_WID
		LEFT JOIN EDW_REP.dbo.DimCitizenshipStatus cs ON cs.CitizenshipStatus_WID = ad.CitizenshipStatus_WID
		LEFT JOIN EDW_REP.dbo.DimDates d ON CONVERT(DATE,d.Date) = CONVERT(DATE, ad.SubmittedDate)
		--LEFT JOIN RaceAndEthnicityVw re ON re.Applicant_BKEY = a.Applicant_BKEY 
		where 
			--a.Etl_Run_Id=@EDWLogID or 
			ad.Etl_Run_Id=@EDWLogID 
			--or o.Etl_Run_Id=@EDWLogID 
			--or AI.Etl_Run_Id=@EDWLogID 
			--or CI.Etl_Run_Id=@EDWLogID 
			--or AY.Etl_Run_Id=@EDWLogID 
			--or fgs.Etl_Run_Id=@EDWLogID 
			--or cs.Etl_Run_Id=@EDWLogID 
			
		 ;

		--------To update submitted dates for applicants to Cycle Start Date + 1 when the Submitted Date is less than or Equal to Cycle Start Date Comment Id --- C1
		UPDATE FA
		SET FA.SubmittedDate_WID = CASE WHEN CAST(CAST(FA.SubmittedDate_WID AS VARCHAR(8)) AS DATE) <= AI.CycleStartDate THEN CAST(CONVERT(VARCHAR(10), DATEADD(DD, 1, AI.CycleStartDate), 112) AS INT) ELSE FA.SubmittedDate_WID END 
		FROM FactApplicationTemp FA
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON FA.AssociationInstance_WID = AI.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID
		WHERE CI.CASName LIKE '%CommonApp%' and FA.Etl_Run_Id=@EDWLogID

-------------------------------------------------------------------Update Race and Ethnicity for Common App Starts------------------------------------------------------------------------------------------
		UPDATE FA SET FA.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -9997 and DataSourceId=3) --'Non Resident Alien'
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		WHERE AD.ISUSResident = 0 AND CI.CASName LIKE '%CommonApp%' and ad.Etl_Run_Id=@EDWLogID

		UPDATE FA SET FA.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -2371 and DataSourceId=3)  --'Hispanic Or Latino'
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		WHERE AD.ISUSResident = 1 AND AD.IsHispanicOrLatino = 1 AND FA.RaceAndEthnicity_WID IS NULL AND CI.CASName LIKE '%CommonApp%' and ad.Etl_Run_Id=@EDWLogID

		UPDATE FA1 SET FA1.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -9996 and DataSourceId=3) --'Two Or More Races'
		FROM 
		FactApplicationTemp FA1 INNER JOIN 
		(SELECT FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID 
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		INNER JOIN (SELECT ED1.* FROM EDW_REP.dbo.DimEthnicityDetails ED1 INNER JOIN (SELECT RE.* FROM EDW_REP.dbo.DimRaceAndEthnicity RE WHERE RE.RaceAndEthnicity NOT IN ('YES','NO')) RE2 ON ED1.Ethnicity_BKEY = RE2.RaceAndEthnicity_BKEY 
		AND ED1.DataSourceId = RE2.DataSourceId) ED 
									ON FA.Applicant_WID = ED.Applicant_WID AND FA.AssociationInstance_WID = ED.AssociationInstance_WID AND FA.Organization_WID = ED.Organization_WID
		INNER JOIN EDW_REP.dbo.DimRaceAndEthnicity RE ON RE.RaceAndEthnicity_BKEY = ED.Ethnicity_BKEY AND RE.DataSourceId = ED.DataSourceId
		WHERE AD.ISUSResident = 1 --AND AD.IsHispanicOrLatino = 0 
		AND FA.RaceAndEthnicity_WID IS NULL AND CI.CASName LIKE '%CommonApp%' and ad.Etl_Run_Id=@EDWLogID
		GROUP BY FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID 
		HAVING COUNT(ED.Ethnicity_BKEY) > 1) FA2 
		ON FA1.Applicant_WID = FA2.Applicant_WID AND FA1.AssociationInstance_WID = FA2.AssociationInstance_WID AND FA1.Organization_WID = FA2.Organization_WID 
		AND FA1.RaceAndEthnicity_WID IS NULL

		UPDATE FA1 SET FA1.RaceAndEthnicity_WID = FA2.RaceAndEthnicity_WID
		FROM 
		FactApplicationTemp FA1 INNER JOIN 
		(SELECT FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID, RE.RaceAndEthnicity_WID 
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		INNER JOIN (SELECT ED1.* FROM EDW_REP.dbo.DimEthnicityDetails ED1 INNER JOIN (SELECT RE.* FROM EDW_REP.dbo.DimRaceAndEthnicity RE WHERE RE.RaceAndEthnicity NOT IN ('YES','NO')) RE2 ON ED1.Ethnicity_BKEY = RE2.RaceAndEthnicity_BKEY
		AND ED1.DataSourceId = RE2.DataSourceId) ED 
									ON FA.Applicant_WID = ED.Applicant_WID AND FA.AssociationInstance_WID = ED.AssociationInstance_WID AND FA.Organization_WID = ED.Organization_WID
		INNER JOIN EDW_REP.dbo.DimRaceAndEthnicity RE ON RE.RaceAndEthnicity_BKEY = ED.Ethnicity_BKEY AND RE.DataSourceId = ED.DataSourceId
		WHERE AD.ISUSResident = 1 --AND AD.IsHispanicOrLatino = 0 
		AND FA.RaceAndEthnicity_WID IS NULL  AND CI.CASName LIKE '%CommonApp%' and ad.Etl_Run_Id=@EDWLogID
		GROUP BY FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID, RE.RaceAndEthnicity_WID 
		HAVING COUNT(ED.Ethnicity_BKEY) = 1) FA2 
		ON FA1.Applicant_WID = FA2.Applicant_WID AND FA1.AssociationInstance_WID = FA2.AssociationInstance_WID AND FA1.Organization_WID = FA2.Organization_WID
		AND FA1.RaceAndEthnicity_WID IS NULL

		UPDATE FA1 SET FA1.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -9998 and DataSourceId=3)  -- 'Did Not Report'
		FROM 
		FactApplicationTemp FA1 INNER JOIN 
		(SELECT FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID  --FA.RaceAndEthnicity_WID --,COUNT(ED.Ethnicity_BKEY)
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		left JOIN (SELECT ED1.* FROM EDW_REP.dbo.DimEthnicityDetails ED1 INNER JOIN (SELECT RE.* FROM EDW_REP.dbo.DimRaceAndEthnicity RE WHERE RE.RaceAndEthnicity NOT IN ('YES','NO')) RE2 ON ED1.Ethnicity_BKEY = RE2.RaceAndEthnicity_BKEY
		AND ED1.DataSourceId = RE2.DataSourceId) ED 
									ON FA.Applicant_WID = ED.Applicant_WID AND FA.AssociationInstance_WID = ED.AssociationInstance_WID AND FA.Organization_WID = ED.Organization_WID
		WHERE AD.ISUSResident = 1 
		and ED.Ethnicity_BKEY is null
		AND FA.RaceAndEthnicity_WID IS NULL 
		AND CI.CASName LIKE '%CommonApp%' and ad.Etl_Run_Id=@EDWLogID
		GROUP BY FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID 
		HAVING COUNT(ED.Ethnicity_BKEY) = 0) FA2 
		ON FA1.Applicant_WID = FA2.Applicant_WID AND FA1.AssociationInstance_WID = FA2.AssociationInstance_WID AND FA1.Organization_WID = FA2.Organization_WID 
		AND FA1.RaceAndEthnicity_WID IS NULL

-------------------------------------------------------------------Update Race and Ethnicity for Common App Ends------------------------------------------------------------------------------------------

-------------------------------------------------------------------Update Race and Ethnicity for Other CAS'es Starts------------------------------------------------------------------------------------------
		--------UPDATE FA SET FA.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -9997) --'Non Resident Alien'
		--------FROM  
		--------FactApplicationTemp FA 
		--------INNER JOINEDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		--------INNER JOINEDW_REP.dbo.AssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		--------INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		--------WHERE AD.ISUSResident = 0 AND FA.RaceAndEthnicity_WID IS NULL

		UPDATE FA SET FA.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -2371 and DataSourceId=3)  --'Hispanic Or Latino'
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		WHERE AD.ISUSResident = 1 AND AD.IsHispanicOrLatino = 1 AND FA.RaceAndEthnicity_WID IS NULL and ad.Etl_Run_Id=@EDWLogID

		UPDATE FA1 SET FA1.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -9996 and DataSourceId=3) --'Two Or More Races'
		FROM 
		FactApplicationTemp FA1 INNER JOIN 
		(SELECT FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID 
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		INNER JOIN (SELECT ED1.* FROM EDW_REP.dbo.DimEthnicityDetails ED1 INNER JOIN (SELECT RE.* FROM EDW_REP.dbo.DimRaceAndEthnicity RE WHERE RE.RaceAndEthnicity NOT IN ('YES','NO')) RE2 ON ED1.Ethnicity_BKEY = RE2.RaceAndEthnicity_BKEY
		AND ED1.DataSourceId = RE2.DataSourceId) ED 
									ON FA.Applicant_WID = ED.Applicant_WID AND FA.AssociationInstance_WID = ED.AssociationInstance_WID AND FA.Organization_WID = ED.Organization_WID
		INNER JOIN EDW_REP.dbo.DimRaceAndEthnicity RE ON RE.RaceAndEthnicity_BKEY = ED.Ethnicity_BKEY AND ED.DataSourceId = RE.DataSourceId
		WHERE AD.ISUSResident = 1 --AND AD.IsHispanicOrLatino = 0 
		AND FA.RaceAndEthnicity_WID IS NULL and ad.Etl_Run_Id=@EDWLogID
		GROUP BY FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID 
		HAVING COUNT(ED.Ethnicity_BKEY) > 1) FA2 
		ON FA1.Applicant_WID = FA2.Applicant_WID AND FA1.AssociationInstance_WID = FA2.AssociationInstance_WID AND FA1.Organization_WID = FA2.Organization_WID 
		AND FA1.RaceAndEthnicity_WID IS NULL

		UPDATE FA1 SET FA1.RaceAndEthnicity_WID = FA2.RaceAndEthnicity_WID
		FROM 
		FactApplicationTemp FA1 INNER JOIN 
		(SELECT FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID, RE.RaceAndEthnicity_WID 
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		INNER JOIN (SELECT ED1.* FROM EDW_REP.dbo.DimEthnicityDetails ED1 INNER JOIN (SELECT RE.* FROM EDW_REP.dbo.DimRaceAndEthnicity RE WHERE RE.RaceAndEthnicity NOT IN ('YES','NO')) RE2 ON ED1.Ethnicity_BKEY = RE2.RaceAndEthnicity_BKEY
		AND ED1.DataSourceId = RE2.DataSourceId) ED 
									ON FA.Applicant_WID = ED.Applicant_WID AND FA.AssociationInstance_WID = ED.AssociationInstance_WID AND FA.Organization_WID = ED.Organization_WID
		INNER JOIN EDW_REP.dbo.DimRaceAndEthnicity RE ON RE.RaceAndEthnicity_BKEY = ED.Ethnicity_BKEY AND ED.DataSourceId = RE.DataSourceId
		WHERE AD.ISUSResident = 1 --AND AD.IsHispanicOrLatino = 0 
		AND FA.RaceAndEthnicity_WID IS NULL and ad.Etl_Run_Id=@EDWLogID
		GROUP BY FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID, RE.RaceAndEthnicity_WID 
		HAVING COUNT(ED.Ethnicity_BKEY) = 1) FA2 
		ON FA1.Applicant_WID = FA2.Applicant_WID AND FA1.AssociationInstance_WID = FA2.AssociationInstance_WID AND FA1.Organization_WID = FA2.Organization_WID
		AND FA1.RaceAndEthnicity_WID IS NULL

		UPDATE FA1 SET FA1.RaceAndEthnicity_WID = (SELECT TOP 1 RaceAndEthnicity_WID FROM EDW_REP.dbo.DimRaceAndEthnicity WHERE RaceAndEthnicity_BKEY = -9998 and DataSourceId=3)  -- 'Did Not Report'
		FROM 
		FactApplicationTemp FA1 INNER JOIN 
		(SELECT FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID  --FA.RaceAndEthnicity_WID --,COUNT(ED.Ethnicity_BKEY)
		FROM  
		FactApplicationTemp FA 
		INNER JOIN EDW_REP.dbo.DimAPPLICANTDETAILS AD ON FA.Applicant_WID = AD.Applicant_WID AND FA.AssociationInstance_WID = AD.AssociationInstance_WID AND FA.Organization_WID = AD.Organization_WID
		INNER JOIN EDW_REP.dbo.DimAssociationInstance AI ON AI.AssociationInstance_WID = FA.AssociationInstance_WID
		INNER JOIN EDW_REP.dbo.DimCASIdentifiers CI ON AI.CAS_WID = CI.CAS_WID 
		left JOIN (SELECT ED1.* FROM EDW_REP.dbo.DimEthnicityDetails ED1 INNER JOIN (SELECT RE.* FROM EDW_REP.dbo.DimRaceAndEthnicity RE WHERE RE.RaceAndEthnicity NOT IN ('YES','NO')) RE2 ON ED1.Ethnicity_BKEY = RE2.RaceAndEthnicity_BKEY
		AND ED1.DataSourceId = RE2.DataSourceId) ED 
									ON FA.Applicant_WID = ED.Applicant_WID AND FA.AssociationInstance_WID = ED.AssociationInstance_WID AND FA.Organization_WID = ED.Organization_WID
		WHERE AD.ISUSResident = 1 
		and ED.Ethnicity_BKEY is null
		AND FA.RaceAndEthnicity_WID IS NULL and ad.Etl_Run_Id=@EDWLogID
		GROUP BY FA.Applicant_WID, FA.AssociationInstance_WID, FA.Organization_WID 
		HAVING COUNT(ED.Ethnicity_BKEY) = 0) FA2 
		ON FA1.Applicant_WID = FA2.Applicant_WID AND FA1.AssociationInstance_WID = FA2.AssociationInstance_WID AND FA1.Organization_WID = FA2.Organization_WID 
		AND FA1.RaceAndEthnicity_WID IS NULL

-------------------------------------------------------------------Update Race and Ethnicity for Other CAS'es Ends------------------------------------------------------------------------------------------

		--Updating the field with -9999 

		UPDATE FactApplicationTemp
		SET RaceAndEthnicity_WID='-9999'
		WHERE RaceAndEthnicity_WID IS NULL;

		UPDATE FactApplicationTemp
		SET ResidenceCountry_WID='-9999'
		WHERE ResidenceCountry_WID IS NULL;

		--UPdate Null state information with -9999
	
		UPDATE FactApplicationTemp
		SET ResidenceState_WID='-9999'
		WHERE ResidenceState_WID IS NULL;

		Update FactApplicationTemp 
		SET StartTerm_WID='-9999' 
		where StartTerm_WID is NULL;


		Update FactApplicationTemp 
		SET CitizenshipStatus_WID='-9999' 
		where CitizenshipStatus_WID is NULL;

		Update FactApplicationTemp 
		SET DecisionPlan_WID='-9999' 
		where DecisionPlan_WID is NULL;

		Update FactApplicationTemp 
		SET FeeWaiverStatus_WID='-9999' 
		where FeeWaiverStatus_WID is NULL;

		Update FactApplicationTemp 
		SET FirstGenStatus_WID='-9999' 
		where FirstGenStatus_WID is NULL;

		Update FactApplicationTemp 
		SET ApplicantType_WID ='-9999'
		where ApplicantType_WID  is null;

		Update FactApplicationTemp 
		SET SubmittedDate_WID ='-9999'
		where SubmittedDate_WID is null;

		Update FactApplicationTemp 
		SET GPAScale_WID ='-9999'
		where GPAScale_WID is null;

		Update FactApplicationTemp 
		SET GPAWeight_WID ='-9999'
		where GPAWeight_WID is null;
		
		Update FactApplicationTemp 
		SET IntendedDegree_WID ='-9999'
		where IntendedDegree_WID is null;

		  -- Merge criteria on WID
		MERGE EDW_REP. EDW_REP.dbo.FactApplication AS Target
		 USING  EDW_REP.dbo.FactApplicationTemp AS Source
			ON Target.Applicant_WID = Source.Applicant_WID AND  
		Target.AssociationInstance_WID = Source.AssociationInstance_WID 
		AND  Target.Organization_WID = Source.Organization_WID
		AND  Target.DataSourceId = Source.DataSourceId
		WHEN MATCHED 
		THEN
		UPDATE SET  
		--Target.Applicant_WID =  Source.Applicant_WID,
		--			Target.AssociationInstance_WID =  Source.AssociationInstance_WID,
		--			Target.Organization_WID =  Source.Organization_WID,
					Target.Institution_WID =  Source.Institution_WID,
					Target.Program_WID =  Source.Program_WID,
					Target.ProgramType_WID =  Source.ProgramType_WID,
					Target.Degree_WID =  Source.Degree_WID,
					Target.Major_WID =  Source.Major_WID,
					Target.ApplicantType_WID =  Source.ApplicantType_WID,
					Target.DecisionPlan_WID =  Source.DecisionPlan_WID,
					Target.FirstGenStatus_WID =  Source.FirstGenStatus_WID,
					Target.FeeWaiverStatus_WID =  Source.FeeWaiverStatus_WID,
					Target.StartTerm_WID =  Source.StartTerm_WID,
					Target.ApplicantStatus_WID =  Source.ApplicantStatus_WID,
					Target.ApplicationStatus_WID =  Source.ApplicationStatus_WID,
					Target.ApplicantDetails_WID =  Source.ApplicantDetails_WID,
					Target.RaceAndEthnicity_WID =  Source.RaceAndEthnicity_WID,
					Target.ApplicationYear =  Source.ApplicationYear,
					Target.SubmittedDate_WID =  Source.SubmittedDate_WID,
					Target.CompletedDate_WID =  Source.CompletedDate_WID,
					Target.VerifiedDate_WID =  Source.VerifiedDate_WID,
					Target.FirstSubmittedDate_WID =  Source.FirstSubmittedDate_WID,
					Target.FirstCompletedDate_WID =  Source.FirstCompletedDate_WID,
					Target.FirstVerifiedDate_WID =  Source.FirstVerifiedDate_WID,
					Target.Gender =  Source.Gender,
					Target.Age =  Source.Age,
					Target.W_Update_Dt =  Source.W_Update_Dt,
					Target.Etl_Run_Id =  Source.Etl_Run_Id,
					Target.CitizenshipStatus_WID =  Source.CitizenshipStatus_WID,
					Target.ResidenceCountry_WID =  Source.ResidenceCountry_WID,
					Target.ResidenceState_WID =  Source.ResidenceState_WID,
					Target.Applicant_ExternalId =  Source.Applicant_ExternalId,
					Target.GPA = Source.GPA,
					Target.GPAScale_WID = Source.GPAScale_WID,
					Target.GPAWeight_WID = Source.GPAWeight_WID,
					Target.IntendedDegree_WID = Source.IntendedDegree_WID,
					Target.PermanentHome_WID = Source.PermanentHome_WID,
					Target.CareerInterest_WID = Source.CareerInterest_WID

		WHEN NOT MATCHED BY TARGET
		THEN 
		INSERT     (Applicant_WID,
					AssociationInstance_WID,
					Organization_WID,
					Institution_WID,
					Program_WID,
					ProgramType_WID,
					Degree_WID,
					Major_WID,
					ApplicantType_WID,
					DecisionPlan_WID,
					FirstGenStatus_WID,
					FeeWaiverStatus_WID,
					StartTerm_WID,
					ApplicantStatus_WID,
					ApplicationStatus_WID,
					ApplicantDetails_WID,
					RaceAndEthnicity_WID,
					ApplicationYear,
					SubmittedDate_WID,
					CompletedDate_WID,
					VerifiedDate_WID,
					FirstSubmittedDate_WID,
					FirstCompletedDate_WID,
					FirstVerifiedDate_WID,
					Gender,
					Age,
					W_Insert_Dt,
					W_Update_Dt,
					Etl_Run_Id,
					CitizenshipStatus_WID,
					ResidenceCountry_WID,
					ResidenceState_WID,
					Applicant_ExternalId,
					GPA,
					GPAScale_WID,
					GPAWeight_WID,
					IntendedDegree_WID,
					PermanentHome_WID,
					CareerInterest_WID,
					DataSourceId
					)
			VALUES  
					(
					Source.Applicant_WID,
					Source.AssociationInstance_WID,
					Source.Organization_WID,
					Source.Institution_WID,
					Source.Program_WID,
					Source.ProgramType_WID,
					Source.Degree_WID,
					Source.Major_WID,
					Source.ApplicantType_WID,
					Source.DecisionPlan_WID,
					Source.FirstGenStatus_WID,
					Source.FeeWaiverStatus_WID,
					Source.StartTerm_WID,
					Source.ApplicantStatus_WID,
					Source.ApplicationStatus_WID,
					Source.ApplicantDetails_WID,
					Source.RaceAndEthnicity_WID,
					Source.ApplicationYear,
					Source.SubmittedDate_WID,
					Source.CompletedDate_WID,
					Source.VerifiedDate_WID,
					Source.FirstSubmittedDate_WID,
					Source.FirstCompletedDate_WID,
					Source.FirstVerifiedDate_WID,
					Source.Gender,
					Source.Age,
					Source.W_Insert_Dt,
					Source.W_Update_Dt,
					Source.Etl_Run_Id,
					Source.CitizenshipStatus_WID,
					Source.ResidenceCountry_WID,
					Source.ResidenceState_WID,
					Source.Applicant_ExternalId,
					Source.GPA,
					Source.GPAScale_WID,
					Source.GPAWeight_WID,
					Source.IntendedDegree_WID,
					Source.PermanentHome_WID,
					Source.CareerInterest_WID,
					Source.DataSourceId
					);

		
		UPDATE a SET A.ActivityCount = COUNT_ACTIVITIES FROM FactApplication A
		INNER JOIN (SELECT DISTINCT APPLICANT_WID, AssociationInstance_WID, COUNT(Activity_BKEY) COUNT_ACTIVITIES                
		 FROM EDW_REP.dbo.DimApplicantActivities
		GROUP BY APPLICANT_WID, AssociationInstance_WID) B ON A.Applicant_WID = B.Applicant_WID AND A.AssociationInstance_WID = B.AssociationInstance_WID
		where A.Etl_Run_Id = @EDWLogID

		
		UPDATE a SET A.ActivityCount = 0 FROM FactApplication A
		Where A.Etl_Run_Id = @EDWLogID AND a.ActivityCount IS NULL

		UPDATE FactApplication
		SET RaceAndEthnicity_WID='-9999'
		WHERE RaceAndEthnicity_WID IS NULL and Etl_Run_Id = @EDWLogID;

		UPDATE FactApplication
		SET ResidenceCountry_WID='-9999'
		WHERE ResidenceCountry_WID IS NULL and Etl_Run_Id = @EDWLogID;

		--UPdate Null state information with -9999
	
		UPDATE FactApplication
		SET ResidenceState_WID='-9999'
		WHERE ResidenceState_WID IS NULL and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET StartTerm_WID='-9999' 
		where StartTerm_WID is NULL and Etl_Run_Id = @EDWLogID;


		Update FactApplication 
		SET CitizenshipStatus_WID='-9999' 
		where CitizenshipStatus_WID is NULL and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET DecisionPlan_WID='-9999' 
		where DecisionPlan_WID is NULL and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET FeeWaiverStatus_WID='-9999' 
		where FeeWaiverStatus_WID is NULL and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET FirstGenStatus_WID='-9999' 
		where FirstGenStatus_WID is NULL and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET ApplicantType_WID ='-9999'
		where ApplicantType_WID  is null and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET SubmittedDate_WID ='-9999'
		where SubmittedDate_WID is null and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET GPAScale_WID ='-9999'
		where GPAScale_WID is null and Etl_Run_Id = @EDWLogID;

		Update FactApplication 
		SET GPAWeight_WID ='-9999'
		where GPAWeight_WID is null and Etl_Run_Id = @EDWLogID;

		UPDATE FactApplication 
		SET IntendedDegree_WID = '-9999'
		where IntendedDegree_WID is null and Etl_Run_Id = @EDWLogID;

		UPDATE FactApplication 
		SET PermanentHome_WID = '-9999'
		where PermanentHome_WID is null and Etl_Run_Id = @EDWLogID;

		UPDATE FactApplication 
		SET CareerInterest_WID = '-9999'
		where CareerInterest_WID is null and Etl_Run_Id = @EDWLogID;
		